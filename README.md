# mw-prog

MegaWiFi Programmer

![mw-prog](/mw-prog.jpg)

This programmer is for the MegaWiFi project. It can write SEGA Genesis/Megadrive ROM files to MegaWiFi cartridges (up to 32 Mbit). It can also upload firmware blobs to the WiFi module inside MegaWiFi cartridges. And the latest version can also write ROM files to FrugalMapper SEGA Master System cartridges. To use the programmer, you will need:

- Programmer bootloader: https://gitlab.com/doragasu/mw-mdma-bl
- Programmer firmware: https://gitlab.com/doragasu/mw-mdma-fw
- Programmer command-line interface: https://gitlab.com/doragasu/mw-mdma-cli

And of course a both a MegaWiFi programmer and the target MegaWiFi or FrugalMapper cartridge.

## License

mw-prog is licensed under the [CERN OHL 1.2](http://www.ohwr.org/licenses/cern-ohl/v1.2) license. You are free to study, distribute and make modifications to the Documentation, under the aforementioned license terms.

## About CAD files

Electronic CAD design files are for KiCAD Open Source electronics design suite. Please install latest version along with KiCAD libraries to open the project. When opening schematic, if KiCAD complains about missing library "doragasu", [you can find it here](https://github.com/doragasu/doragasu-kicad-lib).

Files under `enclosure` directory are for the design and fabrication of the PCB enclosure. You can 3D-print the OBJ files to have a not specially pretty but functional enclosure for the programmer. If you want to edit the design files, you will need FreeCAD.

You can download ready to manufacture Gerber files, BoM, etc. [in the package registry](https://gitlab.com/doragasu/mw-prog/-/packages).

Enjoy!
